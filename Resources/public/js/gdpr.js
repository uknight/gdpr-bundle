/**
 * Here goes GDPR consent script
 */
!function($){
    document.addEventListener("DOMContentLoaded", function() {
        // пилим костылище))
        var c = localStorage.getItem("uknight.gdpr.agreed");
        if (!c){
            document.getElementById('gdpr-cookie-message').classList.add('show');
            document.getElementById('gdpr-cookie-accept').addEventListener('click', function(e){
                e.preventDefault();
                document.getElementById('gdpr-cookie-message').classList.remove('show');
                localStorage.setItem("uknight.gdpr.agreed", true);
            });
        }
    });
}();