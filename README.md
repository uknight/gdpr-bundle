# U-Knight GDPR cookie consent bundle

This bundle is for GDPR cookie consent.

## Instruction

### Prerequisites

You need:

1. symfony
2. brain and straight arms
3. this installation instructions

### Installation

Add to your ```composer.json``` file following lines:

```json
    ...
    "repositories": [
        {
            "url": "https://bitbucket.org/uknight/gdpr-bundle.git",
            "type": "git"
        }
    ]
    ...
```

```json
    "require": {
        ...
        "uknight/gdpr-bundle": "@dev"
    },
```

Then run

```bash
$ composer update
```

### Configuration

Add following lines to your ```AppKernel.php``` file:

```php
$bundles = [
    ...
    new Uknight\GDPRBundle\UknightGDPRBundle(),
];
```

You need to configure ```uknight_gdpr.privacy_link``` or ```uknight_gdpr.privacy_path``` with ```uknight_gdpr.privacy_params``` leaving ```uknight_gdpr.privacy_link``` empty in your ```config.yml``` in order the bundle to work.

```yaml
parameters:
    gdpr:
        header_class: ''
        panel_class: ''
        text_class: ''
        button_class: ''
        use_css: false
        privacy_link: '/privacy-policy' 
        privacy_path: 'custom-page'
        path_params:
            alias: 'privacy-policy'
    
twig:
    globals:
        uknight_gdpr: '%gdpr%'
```

And don't forget to install assets

```bash
$ php bin/console assets:install --symlink
```

## Usage

Just include template provided by the bundle

### Symfony 4
```twig
{{ include('@UknightGDPRBundle::gdpr-cookie-consent.html.twig') }}
```

### Symfony 3
```twig
{{ include('@UknightGDPR/gdpr-cookie-consent.html.twig') }}
```

## Credits

Big thanks to myself, Flash from U-Knight Web Studio LLC [u-knight.de](https://u-knight.de/en/) , for making this bundle ;)